import engine.BoardEvaluator;
import engine.Game;
import engine.GameNode;
import org.junit.jupiter.api.Test;

import java.util.List;

public class GameTests {

    @Test
    public void moveTest() {
        Game game = new Game();
        game.playMove(3, "X");
        assert game.getBoard().getState().get(3).equals("X");
    }

    @Test
    public void evaluationFutureWinTest() {
        Game game = new Game();
        game.playMove(1, "X");
        game.playMove(2, "O");
        game.playMove(4, "X");
        game.playMove(3, "O");
        int evaluationScore = BoardEvaluator.evaluate(game.getBoard(), game.getActivePlayer().getMark());
        assert evaluationScore == -1;
    }

    @Test
    public void evaluationFutureLoseTest() {
        Game game = new Game();
        game.playMove(5, "X");
        game.playMove(4, "O");
        game.playMove(3, "X");
        game.playMove(7, "O");
        game.playMove(1, "X");
        int evaluationScore = BoardEvaluator.evaluate(game.getBoard(), game.getActivePlayer().getMark());
        assert evaluationScore == -1;
    }

    @Test
    public void evaluationFullBoardTest() {
        Game game = new Game();
        game.playMove(1, "X");
        game.playMove(2, "O");
        game.playMove(4, "X");
        game.playMove(7, "O");
        game.playMove(5, "X");
        game.playMove(9, "O");
        game.playMove(3, "X");
        game.playMove(6, "O");
        game.playMove(8, "X");
        int evaluationScore = BoardEvaluator.evaluate(game.getBoard(), game.getActivePlayer().getMark());
        assert evaluationScore == 1;
    }

    @Test
    public void evaluationWinTest() {
        Game game = new Game();
        game.playMove(5, "X");
        game.playMove(4, "O");
        game.playMove(1, "X");
        game.playMove(7, "O");
        game.playMove(9, "X");
        int evaluationScore = BoardEvaluator.evaluate(game.getBoard(), "X");
        assert evaluationScore == 2;
    }

    @Test
    public void evaluationLossTest() {
        Game game = new Game();
        game.playMove(5, "X");
        game.playMove(4, "O");
        game.playMove(1, "X");
        game.playMove(7, "O");
        game.playMove(9, "X");
        int evaluationScore = BoardEvaluator.evaluate(game.getBoard(), "O");
        assert evaluationScore == 0;
    }

    @Test
    public void getPossibleMovesWhenEmptyBoardTest() {
        Game game = new Game();
        var moves = BoardEvaluator.getPossibleMoves(game.getBoard());
        assert moves.size() == 9;
        assert moves.containsAll(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9));
    }

    @Test
    public void getPossibleMovesWhenFullBoardTest() {
        Game game = new Game();
        game.playMove(1, "X");
        game.playMove(2, "O");
        game.playMove(4, "X");
        game.playMove(7, "O");
        game.playMove(5, "X");
        game.playMove(9, "O");
        game.playMove(3, "X");
        game.playMove(6, "O");
        game.playMove(8, "X");
        assert BoardEvaluator.getPossibleMoves(game.getBoard()).isEmpty();
    }

    @Test
    public void getPossibleMovesWhenFourTilesLeft() {
        Game game = new Game();
        game.playMove(5, "X");
        game.playMove(4, "O");
        game.playMove(1, "X");
        game.playMove(7, "O");
        game.playMove(9, "X");
        var moves = BoardEvaluator.getPossibleMoves(game.getBoard());
        assert moves.size() == 4;
        assert moves.containsAll(List.of(2, 3, 6, 8));
    }

    @Test
    public void findBestMoveWinInOneTurnTest() {
        Game game = new Game();
        game.playMove(2, "X");
        game.playMove(1, "O");
        game.playMove(8, "X");
        game.playMove(3, "O");
        BoardEvaluator evaluator = new BoardEvaluator("X");
        GameNode root = new GameNode();
        root.setBoard(game.getBoard());
        int bestMove = evaluator.findNextMove(root, "X").getTargetTile();
        assert bestMove == 5;
    }

//    @Test
//    public void findLeavesTest() {
//        engine.Game game = new engine.Game();
//        game.playMove(5, "X");
//        game.playMove(4, "O");
//        game.playMove(1, "X");
//        game.playMove(7, "O");
//
//        System.out.println(game.getBoard().displayAsText());
//
//        engine.BoardEvaluator evaluator = new engine.BoardEvaluator("X");
//        engine.GameNode root = new engine.GameNode();
//        root.setBoard(game.getBoard());
//        evaluator.findLeaves(root, "X");
//        System.out.println(evaluator.getFinalStates().size());
//        var leaves = evaluator.getLeaves().stream()
//                .filter(leaf -> leaf.getEvaluationScore() == 2).collect(Collectors.toList());
//        System.out.println(leaves.size());
//        for (engine.GameNode leaf: leaves
//        ) {
//            System.out.println("Score: " + leaf.getEvaluationScore());
//            System.out.println("engine.Board state:");
//            System.out.println(leaf.getBoardState() + "\n");
//        }
//    }

    @Test
    public void findLeavesWhenThreeMovesLeftTest() {
        Game game = new Game();
        game.playMove(5, "X");
        game.playMove(4, "O");
        game.playMove(1, "X");
        game.playMove(7, "O");
        game.playMove(2, "X");
        game.playMove(3, "O");

        BoardEvaluator evaluator = new BoardEvaluator("O");
        GameNode root = new GameNode();
        root.setBoard(game.getBoard());
        evaluator.findLeaves(root, "O");
        var leaves = evaluator.getLeaves();
        assert leaves.size() == 4;
    }

    @Test
    public void findNextMoveBasicTest() {
        Game game = new Game();
        game.playMove(5, "X");
        game.playMove(4, "O");
        game.playMove(1, "X");
        game.playMove(7, "O");
        game.playMove(2, "X");
        game.playMove(3, "O");

        System.out.println(game.getBoard().displayAsText());

        BoardEvaluator evaluator = new BoardEvaluator("X");
        GameNode root = new GameNode();
        root.setBoard(game.getBoard());
        GameNode nextMove = evaluator.findNextMove(root, "O");
        System.out.println(nextMove);
        System.out.println(nextMove.getTargetTile());
        System.out.println(nextMove.getEvaluationScore());
    }

    @Test
    public void calculateScoreFourMovesTest() {
        Game game = new Game();
        game.playMove(1, "X");
        game.playMove(3, "O");
        game.playMove(4, "X");
        game.playMove(6, "O");
        game.playMove(8, "X");

        BoardEvaluator evaluator = new BoardEvaluator("O");
        GameNode root = new GameNode();
        root.setBoard(game.getBoard());
        evaluator.findLeaves(root, "X");
        int score = evaluator.calculateScore(root, true);
        assert score == 2;
    }

    @Test
    public void calculateScoreFiveMovesTest() {
        Game game = new Game();
        game.playMove(1, "X");
        game.playMove(8, "O");
        game.playMove(5, "X");
        game.playMove(9, "O");

        System.out.println(game.getBoard().displayAsText());

        BoardEvaluator evaluator = new BoardEvaluator("X");
        GameNode root = new GameNode();
        root.setBoard(game.getBoard());
        evaluator.findLeaves(root, "O");
        int score = evaluator.calculateScore(root, true);
        assert score == 2;
    }

    @Test
    public void calculateScoreEightMovesTest() {
        Game game = new Game();
        game.playMove(5, "X");

        BoardEvaluator evaluator = new BoardEvaluator("O");
        GameNode root = new GameNode();
        root.setBoard(game.getBoard());
        evaluator.findLeaves(root, "X");
        int score = evaluator.calculateScore(root, true);
        assert score == 1;
    }

    @Test
    public void findMoveFiveMovesTest() {
        Game game = new Game();
        game.playMove(1, "X");
        game.playMove(8, "O");
        game.playMove(5, "X");
        game.playMove(9, "O");

        BoardEvaluator evaluator = new BoardEvaluator("X");
        GameNode root = new GameNode();
        root.setBoard(game.getBoard());
        GameNode nextMove = evaluator.findNextMove(root, "X");
        assert nextMove.getTargetTile() == 7;
    }

    @Test
    public void getBoardStateAsStringTest() {
        Game game = new Game();
        game.playMove(1, "X");
        game.playMove(2, "O");
        game.playMove(4, "X");
        game.playMove(7, "O");
        game.playMove(5, "X");
        game.playMove(9, "O");
        game.playMove(3, "X");
        game.playMove(6, "O");
        String state = game.getBoard().getStateAsString();
        System.out.println(state);
        assert state.equals("XOXXXOO.O");
    }

    @Test
    public void setStateFromStringTest() {
        Game game = new Game();
        game.getBoard().setStateFromString("XOXOXO...");
        game.playTurn(7);
        System.out.println(game.getBoard().displayAsText());
        System.out.println(game.getBoard().getStateAsString());
    }
}
