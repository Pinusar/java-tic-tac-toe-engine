import engine.BoardEvaluator;
import engine.Game;
import engine.GameNode;
import engine.IllegalMoveException;

import java.util.Scanner;

public class CommandLineVsPcGame {

    public static void main(String[] args) {
        Game game = new Game();
        System.out.println(game.getBoard().displayAsText());
        BoardEvaluator computerEvaluator = new BoardEvaluator("O");
        while (!game.isOver()) {
            Integer playerTargetTile = getTargetTileInput();
            try { game.playTurn(playerTargetTile); }
            catch (IllegalMoveException e) {
                System.out.println("This move is illegal! Please try again!");
                continue;
            }
            System.out.println(game.getBoard().displayAsText());

            if (!BoardEvaluator.isBoardFull(game.getBoard())) {
                GameNode root = new GameNode();
                root.setBoard(game.getBoard());
                GameNode computerMove = computerEvaluator.findNextMove(root, "O");
                game.playTurn(computerMove.getTargetTile());
                System.out.println(game.getBoard().displayAsText());
            }
        }
        if (game.getWinner().equals("-")) {
            System.out.println("It's a draw!");
        } else {
            System.out.printf("Player %s has won!%n", game.getWinner());
        }
    }

    private static Integer getTargetTileInput() {
        System.out.println("Please enter the number of target tile: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
}
