package engine;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Game {

    private final Board board;
    private final Player player1;
    private final Player player2;
    private Player activePlayer;
    private boolean over;
    private String winner;
    public static final Set<List<Integer>> winningCombinations;

    static {
        winningCombinations = new HashSet<>();
        winningCombinations.add(List.of(1, 2, 3));
        winningCombinations.add(List.of(4, 5, 6));
        winningCombinations.add(List.of(7, 8, 9));
        winningCombinations.add(List.of(1, 4, 7));
        winningCombinations.add(List.of(2, 5, 8));
        winningCombinations.add(List.of(3, 6, 9));
        winningCombinations.add(List.of(1, 5, 9));
        winningCombinations.add(List.of(3, 5, 7));
    }

    public Game() {
        this.board = new Board();
        this.player1 = new Player("X");
        this.player2 = new Player("O");
        this.activePlayer = player1;
        this.over = false;
    }

    public Board getBoard() {
        return board;
    }

    public boolean isOver() {
        return over;
    }

    public Player getActivePlayer() {
        return activePlayer;
    }

    public String getWinner() {
        return winner;
    }

    private void switchActivePlayer() {
        this.activePlayer = this.activePlayer == player1 ? player2 : player1;
    }

    public void playMove(Integer targetTile, String mark) {
        this.board.getState().put(targetTile, mark);
    }

    public void playTurn(Integer targetTile) {
        if (targetTile < 1
                || targetTile > 9
                || this.isOver()
                || !this.board.getState().get(targetTile).equals(".")) {
            throw new IllegalMoveException("The move is illegal!");
        }
        playMove(targetTile, activePlayer.getMark());
        checkWinner();
        switchActivePlayer();
    }

    private void checkWinner() {
        if (BoardEvaluator.isBoardFull(this.board)) {
            this.over = true;
            this.winner = "-";
        }
        for (String playerMark : List.of("X", "O")
        ) {
            if (BoardEvaluator.isWinner(this.board, playerMark)) {
                this.over = true;
                this.winner = playerMark;
            }
        }
    }


}
