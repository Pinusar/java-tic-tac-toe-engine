package engine;

import java.util.*;

public class BoardEvaluator {

    private final List<GameNode> leaves;
    private final List<String> finalStates;
    private final String alliance;

    public BoardEvaluator(String alliance) {
        this.leaves = new ArrayList<>();
        this.finalStates = new ArrayList<>();
        this.alliance = alliance;
    }

    public List<GameNode> getLeaves() {
        return leaves;
    }

    public List<String> getFinalStates() {
        return finalStates;
    }

    public static Integer evaluate(Board board, String playerMark) {
        /*
        0 - Loss,
        1 - Draw,
        2 - Win,
         */
        String opponent = playerMark.equals("X")? "O" : "X";
        if (isWinner(board, playerMark)) {
            return 2;
        }
        else if (isWinner(board, opponent)) {
            return 0;
        }
        else if (isBoardFull(board)) {
            return 1;
        }
        else {
            return -1;
        }
    }

    public void findLeaves(GameNode node, String playerMark) {
        Board board = node.getBoard();
        if (isGameOver(board)) {
            this.finalStates.add(board.displayAsText());
            Integer score = evaluate(board, this.alliance);
            node.setEvaluationScore(score);
            node.setBoardState(board.displayAsText());
            this.leaves.add(node);
        } else {
            playerMark = playerMark.equals("X")? "O" : "X";
            Set<Integer> possibleMoves = getPossibleMoves(board);
            for (Integer targetTile : possibleMoves
            ) {
                HashMap<Integer, String> newState = (HashMap<Integer, String>) board.getState().clone();
                newState.put(targetTile, playerMark);
                Board simulationBoard = new Board(newState);
                GameNode childNode = new GameNode();
                childNode.setTargetTile(targetTile);
                childNode.setBoard(simulationBoard);
                childNode.setBoardState(simulationBoard.displayAsText());
                node.getChildren().add(childNode);
                findLeaves(childNode, playerMark);
            }
        }
    }

    public GameNode findNextMove(GameNode node, String playerMark) {
        findLeaves(node, playerMark.equals("X")? "O" : "X");
        calculateScore(node, true);
        Board board = node.getBoard();
        if (isGameOver(board)) {
            throw new RuntimeException("engine.Game is already over!");
        } else {
            return findHighestScoreNode(node.getChildren());
        }
    }

    public Integer calculateScore(GameNode node, boolean isMaximizing) {
        Board board = node.getBoard();
        if (isGameOver(board)) {
            node.setEvaluationScore(evaluate(board, this.alliance));
            return evaluate(board, this.alliance);
        } else {
            for (GameNode child: node.getChildren()
                 ) {
                child.setEvaluationScore(calculateScore(child, !isMaximizing));
            }
            if (isMaximizing) {
                node.setEvaluationScore(findHighestScore(node.getChildren()));
            } else {
                node.setEvaluationScore(findLowestScore(node.getChildren()));
            }
            return node.getEvaluationScore();
        }
    }

    private Integer findLowestScore(List<GameNode> children) {
        GameNode node = children.stream().min(Comparator.comparing(GameNode::getEvaluationScore)).orElseThrow();
        return node.getEvaluationScore();
    }

    private Integer findHighestScore(List<GameNode> children) {
        GameNode node = children.stream().max(Comparator.comparing(GameNode::getEvaluationScore)).orElseThrow();
        return node.getEvaluationScore();
    }

    private GameNode findHighestScoreNode(List<GameNode> children) {
        return children.stream().max(Comparator.comparing(GameNode::getEvaluationScore)).orElseThrow();
    }

    private static boolean isGameOver(Board board) {
        return isBoardFull(board) || isWinner(board, "X") || isWinner(board, "O");
    }

    public static Set<Integer> getPossibleMoves(Board board) {
        Set<Integer> possibleMoves = new HashSet<>();
        var boardState = board.getState();
        for (var entry: boardState.entrySet()
        ) {
            if (entry.getValue().equals(".")) {
                possibleMoves.add(entry.getKey());
            }
        }
        return possibleMoves;
    }

    public static boolean isBoardFull(Board board) {
        var boardState = board.getState();
        for (var entry: boardState.entrySet()
             ) {
            if (entry.getValue().equals(".")) {
                return false;
            }
        }
        return true;
    }

    public static boolean isWinner(Board board, String playerMark) {
        for (List<Integer> combination : Game.winningCombinations
        ) {
            if (board.getState().get(combination.get(0)).equals(playerMark)
                    && board.getState().get(combination.get(1)).equals(playerMark)
                    && board.getState().get(combination.get(2)).equals(playerMark)) {
                return true;
            }
        }
        return false;
    }
}
