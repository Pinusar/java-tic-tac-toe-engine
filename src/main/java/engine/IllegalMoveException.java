package engine;

public class IllegalMoveException extends IllegalArgumentException {
    public IllegalMoveException(String s) {
        super(s);
    }
}
