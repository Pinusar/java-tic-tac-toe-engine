import engine.Game;

import java.util.Scanner;

public class CommandLinePvPGame {

    public static void main(String[] args) {
        Game game = new Game();
        System.out.println(game.getBoard().displayAsText());
        while (!game.isOver()) {
            Integer targetTile = getTargetTileInput();
            game.playTurn(targetTile);
            System.out.println(game.getBoard().displayAsText());
        }
        System.out.printf("engine.Player %s has lost!%n", game.getActivePlayer().getMark());
    }

    private static Integer getTargetTileInput() {
        System.out.println("Please enter the number of target tile: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
}
